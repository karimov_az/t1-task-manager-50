package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.domain.DatabaseSchemeUpdateRequest;
import ru.t1.karimov.tm.enumerated.Role;

public final class DatabaseSchemeUpdateCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Update database scheme.";

    @NotNull
    public static final String NAME = "database-update-scheme";

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().updateDatabaseScheme(new DatabaseSchemeUpdateRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
