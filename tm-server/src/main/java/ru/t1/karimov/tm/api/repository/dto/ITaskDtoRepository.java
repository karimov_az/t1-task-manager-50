package ru.t1.karimov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    @NotNull
    TaskDto create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    TaskDto create(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

}
